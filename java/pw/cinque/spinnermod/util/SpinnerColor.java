package pw.cinque.spinnermod.util;

import java.awt.Color;

public class SpinnerColor
{
    private static int[] COLORS;
    private float red;
    private float green;
    private float blue;
    private int index;
    
    public SpinnerColor() {
        this.index = 6;
        this.red = 1.0f;
        this.green = 1.0f;
        this.blue = 1.0f;
    }
    
    public SpinnerColor(final float red, final float green, final float blue) {
        this.index = 6;
        this.red = 1.0f;
        this.green = 1.0f;
        this.blue = 1.0f;
        this.red = red;
        this.green = green;
        this.blue = blue;
    }
    
    public void update() {
        Color color;
        if (this.index == 6) {
            color = Color.getHSBColor((float)(System.currentTimeMillis() % 1000L) / 1000.0f, 0.8f, 0.8f);
        }
        else {
            final String colorStr = String.format("#%06X", 0xFFFFFF & SpinnerColor.COLORS[this.index]);
            color = new Color(Integer.valueOf(colorStr.substring(1, 3), 16), Integer.valueOf(colorStr.substring(3, 5), 16), Integer.valueOf(colorStr.substring(5, 7), 16));
        }
        this.red = color.getRed() / 255.0f;
        this.green = color.getGreen() / 255.0f;
        this.blue = color.getBlue() / 255.0f;
    }
    
    public int getIndex() {
        return this.index;
    }
    
    public void setIndex(final int index) {
        this.index = index;
    }
    
    public void next() {
        ++this.index;
        if (this.index == 7) {
            this.index = 0;
        }
    }
    
    public float getRed() {
        return this.red;
    }
    
    public void setRed(final float red) {
        this.red = red;
    }
    
    public float getGreen() {
        return this.green;
    }
    
    public void setGreen(final float green) {
        this.green = green;
    }
    
    public float getBlue() {
        return this.blue;
    }
    
    public void setBlue(final float blue) {
        this.blue = blue;
    }
    
    static {
        SpinnerColor.COLORS = new int[] { 16777215, 16711680, 65280, 255, 16776960, 11141290 };
    }
}
