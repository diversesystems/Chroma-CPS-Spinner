package pw.cinque.spinnermod.util;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

public class ClickCounter
{
    private List<Long> clicks;
    
    public ClickCounter() {
        this.clicks = new ArrayList<Long>();
    }
    
    public void addClick() {
        this.clicks.add(System.currentTimeMillis() + 1000L);
    }
    
    public int getCPS() {
        final Iterator<Long> iterator = this.clicks.iterator();
        while (iterator.hasNext()) {
            if (iterator.next() < System.currentTimeMillis()) {
                iterator.remove();
            }
        }
        return this.clicks.size();
    }
}
