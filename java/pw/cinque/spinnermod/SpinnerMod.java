package pw.cinque.spinnermod;

import java.io.OutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.File;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.gui.GuiScreen;
import pw.cinque.spinnermod.render.SpinnerGui;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraft.command.ICommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import pw.cinque.spinnermod.util.SpinnerColor;
import pw.cinque.spinnermod.render.SpinnerRenderer;
import net.minecraftforge.fml.common.Mod;

@Mod(name = "SpinnerMod v2", modid = "cpsmod", version = "2.0")
public class SpinnerMod
{
    private static SpinnerMod instance;
    private static SpinnerRenderer renderer;
    private boolean showGui;
    private int x;
    private int y;
    private SpinnerColor color;
    
    public SpinnerMod() {
        this.showGui = false;
        this.x = 0;
        this.y = 0;
        this.color = new SpinnerColor();
    }
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        SpinnerMod.instance = this;
        SpinnerMod.renderer = new SpinnerRenderer();
        FMLCommonHandler.instance().bus().register((Object)this);
        FMLCommonHandler.instance().bus().register((Object)SpinnerMod.renderer);
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandSpinnerMod());
        new Thread(() -> SpinnerMod.renderer.runUpdateLoop()).start();
        this.loadSettings();
    }
    
    @SubscribeEvent
    public void onClientTick(final TickEvent.ClientTickEvent event) {
        if (this.showGui) {
            Minecraft.getMinecraft().displayGuiScreen((GuiScreen)new SpinnerGui());
            this.showGui = false;
        }
    }
    
    public static SpinnerMod getInstance() {
        return SpinnerMod.instance;
    }
    
    public static SpinnerRenderer getRenderer() {
        return SpinnerMod.renderer;
    }
    
    public void loadSettings() {
        final File file = new File("config/spinnermod.dat");
        if (!file.exists()) {
            return;
        }
        try (final DataInputStream in = new DataInputStream(new FileInputStream(file))) {
            this.x = in.readInt();
            this.y = in.readInt();
            (this.color = new SpinnerColor(in.readFloat(), in.readFloat(), in.readFloat())).setIndex(in.readInt());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void saveSettings() {
        try (final DataOutputStream out = new DataOutputStream(new FileOutputStream("config/spinnermod.dat", false))) {
            out.writeInt(this.x);
            out.writeInt(this.y);
            out.writeFloat(this.color.getRed());
            out.writeFloat(this.color.getGreen());
            out.writeFloat(this.color.getBlue());
            out.writeInt(this.color.getIndex());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void showGUI() {
        this.showGui = true;
    }
    
    public int getX() {
        return this.x;
    }
    
    public void setX(final int x) {
        this.x = x;
    }
    
    public int getY() {
        return this.y;
    }
    
    public void setY(final int y) {
        this.y = y;
    }
    
    public SpinnerColor getColor() {
        return this.color;
    }
}
