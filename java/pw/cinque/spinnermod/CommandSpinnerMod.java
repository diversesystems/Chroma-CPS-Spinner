package pw.cinque.spinnermod;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.CommandBase;
import net.minecraft.server.MinecraftServer;

public class CommandSpinnerMod extends CommandBase
{
    public String getCommandName() {
        return "spinnermod";
    }
    
    public String getCommandUsage(final ICommandSender p_71518_1_) {
        return "/spinnermod";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        SpinnerMod.getInstance().showGUI();
    }
    
    public int getRequiredPermissionLevel() {
        return 0;
    }
    
    public boolean func_71519_b(final ICommandSender p_71519_1_) {
        return true;
    }
}
