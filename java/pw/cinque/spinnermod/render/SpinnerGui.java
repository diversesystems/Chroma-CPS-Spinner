package pw.cinque.spinnermod.render;

import pw.cinque.spinnermod.SpinnerMod;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class SpinnerGui extends GuiScreen
{
    private final Minecraft mc;
    private boolean dragging;
    private int lastMouseX;
    private int lastMouseY;
    private long lastClick;
    
    public SpinnerGui() {
        this.mc = Minecraft.getMinecraft();
        this.dragging = false;
        this.lastClick = 0L;
    }
    
    public void drawScreen(final int x, final int y, final float partialTicks) {
        this.drawCenteredString(this.mc.fontRendererObj, "SpinnerMod by Fyu", this.width / 2, 2, -1);
        this.drawCenteredString(this.mc.fontRendererObj, "§eClick + drag §fto move.", this.width / 2, 22, -1);
        this.drawCenteredString(this.mc.fontRendererObj, "§eDouble-click §fto select a random color.", this.width / 2, 32, -1);
        try {
            this.handleInput();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        SpinnerMod.getRenderer().drawSpinner();
    }
    
    protected void mouseClicked(final int mouseX, final int mouseY, final int button) {
        if (button != 0) {
            return;
        }
        final int startX = SpinnerMod.getInstance().getX();
        final int startY = SpinnerMod.getInstance().getY();
        final int endX = startX + 128;
        final int endY = startY + 128;
        if (mouseX >= startX && mouseX <= endX && mouseY >= startY && mouseY <= endY) {
            if (System.currentTimeMillis() < this.lastClick + 250L) {
                SpinnerMod.getInstance().getColor().next();
                this.lastClick = 0L;
                return;
            }
            this.dragging = true;
            this.lastMouseX = mouseX;
            this.lastMouseY = mouseY;
            this.lastClick = System.currentTimeMillis();
        }
    }
    
    protected void mouseReleased(final int mouseX, final int mouseY, final int action) {
        this.dragging = false;
    }
    
    protected void mouseClickMove(final int mouseX, final int mouseY, final int lastButtonClicked, final long timeSinceMouseClick) {
        if (!this.dragging) {
            return;
        }
        SpinnerMod.getInstance().setX(SpinnerMod.getInstance().getX() + mouseX - this.lastMouseX);
        SpinnerMod.getInstance().setY(SpinnerMod.getInstance().getY() + mouseY - this.lastMouseY);
        this.lastMouseX = mouseX;
        this.lastMouseY = mouseY;
    }
    
    public boolean doesGuiPauseGame() {
        return false;
    }
    
    public void onGuiClosed() {
        SpinnerMod.getInstance().saveSettings();
    }
}
