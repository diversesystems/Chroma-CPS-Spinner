package pw.cinque.spinnermod.render;

import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.Tessellator;
import net.minecraftforge.client.event.MouseEvent;
import pw.cinque.spinnermod.util.SpinnerColor;
import org.lwjgl.opengl.GL11;
import pw.cinque.spinnermod.SpinnerMod;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.input.Mouse;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import pw.cinque.spinnermod.util.ClickCounter;
import net.minecraft.util.ResourceLocation;
import net.minecraft.client.Minecraft;

public class SpinnerRenderer
{
    private static final float FRICTION = 0.1f;
    private final Minecraft mc;
    private final ResourceLocation spinnerTexture;
    private final ClickCounter clickCounter;
    private volatile float angle;
    private volatile float speed;
    private volatile float acceleration;
    
    public SpinnerRenderer() {
        this.mc = Minecraft.getMinecraft();
        /*
        Replace the picture inside the resources folder with any picture you want and rename it to spinner.png for a
        custom spinning picture instead of a fidget spinner
         */
        this.spinnerTexture = new ResourceLocation("cpsmod", "spinner.png");
        this.clickCounter = new ClickCounter();
        this.angle = 0.0f;
        this.speed = 0.0f;
        this.acceleration = 0.0f;
    }
    
    @SubscribeEvent
    public void onClick(final MouseEvent event) {
        if (event.getButton() != 0) {
            return;
        }

        if (event.isButtonstate()) {
            this.clickCounter.addClick();
            this.acceleration = Math.min(12.0f, this.acceleration + 2.0f);
        }
    }
    
    @SubscribeEvent
    public void onRenderTick(final TickEvent.RenderTickEvent event) {
        if (!this.mc.inGameHasFocus || this.mc.gameSettings.showDebugInfo) {
            return;
        }
        this.drawSpinner();
    }
    
    public void drawSpinner() {
        this.mc.getTextureManager().bindTexture(this.spinnerTexture);
        final ScaledResolution res = new ScaledResolution(this.mc);
        final SpinnerColor color = SpinnerMod.getInstance().getColor();
        color.update();
        int x = SpinnerMod.getInstance().getX();
        int y = SpinnerMod.getInstance().getY();
        if (x < 0) {
            SpinnerMod.getInstance().setX(x = 0);
        }
        else if (x > res.getScaledWidth() - 128) {
            SpinnerMod.getInstance().setX(x = res.getScaledWidth() - 128);
        }
        if (y < 0) {
            SpinnerMod.getInstance().setY(y = 0);
        }
        else if (y > res.getScaledHeight() - 128) {
            SpinnerMod.getInstance().setY(y = res.getScaledHeight() - 128);
        }
        GL11.glPushMatrix();
        GL11.glTranslatef((float)x + 64.0f, (float)y + 64.0f, 0.0f);
        GL11.glRotatef(this.angle, 0.0f, 0.0f, 1.0f);
        GL11.glTranslatef(-64.0f, -64.0f, 0.0f);
        GL11.glColor4f(color.getRed(), color.getGreen(), color.getBlue(), 1.0f);
        this.drawTexturedModalRect(0, 0, 0, 0, 128, 128);
        GL11.glPopMatrix();
        final String text = String.valueOf(this.clickCounter.getCPS());
        this.mc.fontRendererObj.drawStringWithShadow(text, (float)(x + 64 - this.mc.fontRendererObj.getStringWidth(text) / 2), (float)(y + 60), -1);
    }
    
    public void drawTexturedModalRect(final int x, final int y, final int textureX, final int textureY, final int width, final int height) {
        final float f = 1.0f / width;
        final float f2 = 1.0f / height;
        final Tessellator tessellator = Tessellator.getInstance();
        final VertexBuffer worldrenderer = tessellator.getBuffer();
        worldrenderer.begin(7, DefaultVertexFormats.POSITION_TEX);
        worldrenderer.pos((double)(x + 0), (double)(y + height), 0.0).tex((double)((textureX + 0) * f), (double)((textureY + height) * f2)).endVertex();
        worldrenderer.pos((double)(x + width), (double)(y + height), 0.0).tex((double)((textureX + width) * f), (double)((textureY + height) * f2)).endVertex();
        worldrenderer.pos((double)(x + width), (double)(y + 0), 0.0).tex((double)((textureX + width) * f), (double)((textureY + 0) * f2)).endVertex();
        worldrenderer.pos((double)(x + 0), (double)(y + 0), 0.0).tex((double)((textureX + 0) * f), (double)((textureY + 0) * f2)).endVertex();
        tessellator.draw();
    }
    
    public void runUpdateLoop() {
        while (true) {
            this.acceleration = Math.max(0.0f, this.acceleration - 0.1f);
            this.speed = (2.0f * this.speed + this.acceleration) / 3.0f;
            this.angle += this.speed;
            try {
                Thread.sleep(10L);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
